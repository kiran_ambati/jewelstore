Jewel street
============

to create database
update details in 

app/config/parameters.yml

and run

php app/console doctrine:schema:create



Create users using console using following commands

php app/console fos:user:create


There are 2 roles in thia pp

ROLE_USER ( Shopkeeper )

ROLE_ADMIN ( admin )

php app/console fos:user:promote  for admin role


### Login screen of Shopkeeper

![picture](images/login.png)


### Shopkeeper clockin screen

Can see cumulative amount sold in current month
***********************************************

![picture](images/clockin.png)

### clouckout screen of Shopkeeper 

can enter quantity before clockout
*********************************

![picture](images/cloukout.png)

### After clouckout screen of Shopkeeper

Summary of the day
******************

![picture](images/clockedout.png)

###Admin screen

Glance of all shopkeepers with amount of time spent and sold in Grams and in USD
*********************************************************************************
![picture](images/admin.png)

###Goldrates API

custom API give goldrates of current date in json formate
**********************************************************

![picture](images/goldratesAPI.png)
