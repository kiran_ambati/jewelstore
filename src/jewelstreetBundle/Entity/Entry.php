<?php

namespace jewelstreetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entry
 *
 * @ORM\Table(name="entry")
 * @ORM\Entity(repositoryClass="jewelstreetBundle\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clockin", type="datetimetz", nullable=true)
     */
    private $clockin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clockout", type="datetimetz", nullable=true)
     */
    private $clockout;


    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;


    /**
     * @var \jewelstreetBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="jewelstreetBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     * })
     */
    private $users;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clockin
     *
     * @param \DateTime $clockin
     * @return Entry
     */
    public function setClockin($clockin)
    {
        $this->clockin = $clockin;

        return $this;
    }

    /**
     * Get clockin
     *
     * @return \DateTime
     */
    public function getClockin()
    {
        return $this->clockin;
    }

    /**
     * Set clockout
     *
     * @param \DateTime $clockout
     * @return Entry
     */
    public function setClockout($clockout)
    {
        $this->clockout = $clockout;

        return $this;
    }

    /**
     * Get clockout
     *
     * @return \DateTime
     */
    public function getClockout()
    {
        return $this->clockout;
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Entry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = (int)$quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set users
     *
     * @param \jewelstreetBundle\Entity\User $users
     *
     * @return Employees
     */
    public function setUsers(\jewelstreetBundle\Entity\User $users = null)
    {
        $this->users = $users;
        return $this;
    }
    /**
     * Get users
     *
     * @return \jewelstreetBundle\Entity\User
     */
    public function getUsers()
    {
        return $this->users;
    }

}
