<?php

namespace jewelstreetBundle;

use FOS\UserBundle\FOSUserBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class jewelstreetBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
