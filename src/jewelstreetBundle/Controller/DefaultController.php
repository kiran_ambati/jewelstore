<?php

namespace jewelstreetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('jewelstreetBundle:Default:index.html.twig');
    }
}
