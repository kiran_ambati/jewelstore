<?php

namespace jewelstreetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use jewelstreetBundle\Entity\Entry;
use jewelstreetBundle\Form\EntryType;

//use \DateTime;
/**
 * Entry controller.
 *
 */
class EntryController extends Controller
{
    /**
     * Lists all Entry entities.
     *
     */
    public function indexAction(Request $request)
    {
//        $cont = $this->container->getParameter('security.role_hierarchy.roles');
        $user = $this->get('security.context')->getToken()->getUser();
//        $roles = $user->getRoles();
        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository('jewelstreetBundle:Entry')->findTodayEntry($user);
        if ($entries) {
            $entry = $entries[0];
        } else {
            $entry = new Entry();
        }
        if ($request->isMethod('POST')) {
            $postdata = $request->request->get('entry');
            $quantity = $request->request->get('quantity');

            if ($postdata == 'clockout') {
                $entry->setQuantity($quantity);
                $entry->setClockout(new \DateTime());
            } elseif ($postdata == 'clockin') {
                $entry->setClockin(new \DateTime());
                $entry->setUsers($user);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($entry);
            $em->flush();
        }
        $monthsCount = $em->getRepository('jewelstreetBundle:Entry')->findMonthsCount($user->getId());
        $goldrate = json_decode($this->getGoldRate(), true);
        return $this->render('jewelstreetBundle:entry:index.html.twig', array(
            'entry' => $entry,
            'montlysoldgms' => $monthsCount,
            'montlysoldamount' => $monthsCount['total'] * $goldrate['price_per_gram'],
            'username' => $user->getUsername()
        ));
    }

    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            $reports = $em->getRepository('jewelstreetBundle:Entry')->reports();
            $goldrate = json_decode($this->getGoldRate(), true);
            for ($i = 0; $i < count($reports); $i++) {
                $reports[$i]['total_time_in_seconds'] = $this->secondsToReadable($reports[$i]['total_time_in_seconds']);
                $reports[$i]['total_amount'] = ($reports[$i]['total_quantity'] * $goldrate['price_per_gram']).'USD';
            }
            return $this->render('jewelstreetBundle:entry:admin.html.twig', array(
                'reports' => $reports
            ));
        } else {
            return $this->redirect($this->generateUrl('entry_index'));
        }

    }

    private function getGoldRate()
    {
        $buzz = $this->container->get('buzz');
        $response = $buzz->get('http://democentral.xyz/goldrates');
        return $response->getContent();
    }

    private function secondsToReadable($total_seconds)
    {
        $hours = floor($total_seconds / 3600);
        $minutes = floor(($total_seconds / 60) % 60);
        $seconds = $total_seconds % 60;

        return "$hours Hours $minutes minutes $seconds seconds";

    }

}
